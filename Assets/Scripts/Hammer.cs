﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer : MonoBehaviour {

    private bool canRemove = true;
    public GameObject camera;


    // Use this for initialization
    void Start()
    {
        camera = this.transform.parent.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1) && canRemove)
        {
            canRemove = false;
            Remove();
        }
        else if (!Input.GetKey(KeyCode.Mouse1))
        {
            canRemove = true;
        }
    }

    private void Remove()
    {
        RaycastHit col;
        LayerMask layer = 1 << 11;
        if (Physics.Raycast(new Vector3(camera.transform.GetChild(0).position.x, camera.transform.GetChild(0).position.y, camera.transform.GetChild(0).position.z), camera.transform.forward, out col, 4, layer))
        {
            GameObject.Destroy(col.transform.parent.gameObject);
        }
    }
}
