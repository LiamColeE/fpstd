﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipedItem : MonoBehaviour {

    private int equipedItem = 0;
    public int items = 3;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            equipedItem -= 1;
            if (equipedItem < 0)
            {
                equipedItem = items;
            }
            SwitchItem();
        }
        else if(Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            equipedItem += 1;
            if (equipedItem > items)
            {
                equipedItem = 0;
            }
            SwitchItem();
        }
	}

    private void SwitchItem()
    {
        for (int i = 0; i < items +1; i++)
        {
            if(i == equipedItem)
            {
                
                this.transform.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                this.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}
