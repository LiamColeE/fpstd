﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M4 : MonoBehaviour {

    //Gun Changeable statistics
    public int BASE_DAMAGE = 40;
    public int MAX_AMMO = 30;
    public bool FULL_AUTO = true;
    [Range(0, .1f)]
    public float BULLET_SPREAD = 0f;
    [Range(0, .1f)]
    public float MOVEMENT_INNACURACY = 0f;
    private float movementInnacuracy = 0f;
    [Range(0, .01f)]
    public float PER_BULLET_SPRAY_INNACURACY = 0f;
    private float SPRAY_INNACURACY = 0f;

    //bullet
    public LineRenderer lr;
    private bool canShoot = true;
    private bool reloading;
    private int ammoCount = 30;
    private ParticleSystem muzzleFlash;
    private GameObject crossHair;
    private Camera camera;

    // Use this for initialization
    void Start () {
        camera = Camera.main;
        lr = this.gameObject.GetComponent<LineRenderer>();
        lr.enabled = false;
        muzzleFlash = GameObject.Find("Muzzle Flash").GetComponent<ParticleSystem>();
        crossHair = GameObject.Find("CrossHair");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Mouse0) && canShoot && !reloading && ammoCount > 0)
        {
            lr.enabled = true;
            Invoke("RemoveBullet",.03f);
            Fire();
            canShoot = false;
            if (FULL_AUTO)
            {
                Invoke("FullAuto", .1f);
            }
        }
        else if (!Input.GetKey(KeyCode.Mouse0))
        {
            SPRAY_INNACURACY = 0;
            canShoot = true;
        }

        if (Input.GetKey(KeyCode.R) && !reloading && ammoCount != MAX_AMMO)
        {
            reloading = true;
            Invoke("Reload", 3f);
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Aim();
        }
        else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            HipFire();
        }
	}

    private void Fire()
    {
        SPRAY_INNACURACY += PER_BULLET_SPRAY_INNACURACY;
        ammoCount--;
        muzzleFlash.Play();
        Invoke("StopFlash", .05f);
        RaycastHit hit;

        Vector3 bulletDirection = this.transform.parent.parent.forward;
        bulletDirection += Random.insideUnitSphere * (BULLET_SPREAD + MOVEMENT_INNACURACY + SPRAY_INNACURACY);


        Physics.Raycast(this.transform.parent.position,bulletDirection, out hit, 100f);

        lr.SetPosition(0, this.transform.GetChild(0).position);
        lr.SetPosition(1, this.transform.parent.position + bulletDirection * 100f);

        if (hit.distance != 0)
        {
            if (hit.collider.gameObject.tag == "Enemy")
            {
                float damage = BASE_DAMAGE / (hit.distance / 25);
                if (damage > BASE_DAMAGE)
                {
                    damage = BASE_DAMAGE;
                }
                hit.collider.gameObject.GetComponent<EnemyController>().Damage(damage);
            }
            else
            {
                //spawn decal at hit location
                Debug.Log("hit, not enemy");
            }
        }
    }

    private void CeaseFire()
    {

    }

    private void FullAuto()
    {
        canShoot = true;
    }

    private void RemoveBullet()
    {
        lr.enabled = false;
    }

    private void Reload()
    {
        reloading = false;
        ammoCount = MAX_AMMO;
    }

    private void CancelReload()
    {
        CancelInvoke("Reload");
    }

    private void OnDisable()
    {
        reloading = false;
        CancelReload();
    }

    private void StopFlash()
    {
        muzzleFlash.Stop();
    }

    private void Aim()
    {
        this.transform.localPosition = new Vector3(0, -.113f, .18f);
        crossHair.SetActive(false);
        camera.fieldOfView -= 20;
    }

    private void HipFire()
    {
        this.transform.localPosition = new Vector3(0.05f, -.13f, .18f);
        crossHair.SetActive(true);
        camera.fieldOfView += 20;
    }

    public int AmmoCount()
    {
        return ammoCount;
    }

}
