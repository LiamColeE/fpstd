﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    private Rigidbody rb;
    private Vector3 speed = new Vector3(0, 0, 2500f);

	// Use this for initialization
	void Start () {
        rb = this.gameObject.GetComponent<Rigidbody>();
        rb.AddRelativeForce(speed);

        Invoke("DestroyBullet", 3);
	}

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }

    private void DestroyBullet()
    {
        Destroy(this.gameObject);
    }
}
