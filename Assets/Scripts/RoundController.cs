﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RoundController : MonoBehaviour {

    public const float DefaultRoundTime = 60;
    public const float DefaultCoolTime = 5;
    public Spawner spawner;

    private float roundStartTime;
    private bool isCool = true;
    private int round = 10;

    private TextMeshProUGUI timer;
    private TextMeshProUGUI roundCounter;




    // Use this for initialization
    void Start () {
        timer = GameObject.Find("RoundTimer").GetComponent<TextMeshProUGUI>();
        roundCounter = GameObject.Find("RoundCounter").GetComponent<TextMeshProUGUI>();
        roundStartTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        UpdateTimer();
        TimerCheck();

        if (!isCool)
        {
            spawner.Spawn(Random.Range(0,round+1));
        }
        else
        {
        }
	}

    private void TimerCheck()
    {
        if (isCool)
        {
            if((roundStartTime + DefaultCoolTime) < Time.time)
            {
                isCool = false;
                roundStartTime = Time.time;
                round++;
                roundCounter.text = ("Round:" + round);

            }
        }

        else if (!isCool)
        {
            if((roundStartTime + DefaultRoundTime) < Time.time){
                isCool = true;
                roundStartTime = Time.time;
            }
        }
    }

    private void UpdateTimer()
    {
        if (isCool)
        {
            timer.text = ("Cool Down:" + ((roundStartTime + DefaultCoolTime) - Time.time).ToString("n0"));

        }
        else if (!isCool)
        {
            timer.text = ("Time:" + ((roundStartTime + DefaultRoundTime) - Time.time).ToString("n0"));
        }
    }
}
