﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManger : MonoBehaviour
{

    GameObject equipedItems;
    GameObject equipedUI;
    GameObject unequipedItems;
    GameObject unequipedUI;
    // Start is called before the first frame update
    void Start()
    {
        equipedItems = GameObject.Find("EquipedItems");
        equipedUI = this.transform.GetChild(0).gameObject;
        unequipedItems = GameObject.Find("NonEquipedItems");
        unequipedUI = this.transform.GetChild(1).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            unequipedUI.SetActive(true);
        }
        else if (Input.GetKeyUp(KeyCode.Tab))
        {
            unequipedUI.SetActive(false);
        }

        for (int i = 0; i < equipedItems.transform.childCount; i ++)
        {
            equipedUI.transform.GetChild(i).gameObject.GetComponent<RawImage>().texture = equipedItems.transform.GetChild(i).GetChild(0).  gameObject.GetComponent<Icon>().icon;
        }
    }
}
