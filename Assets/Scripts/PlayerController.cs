﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //Player stats
    private float health = 100;
    private RaycastHit hit;

    bool StoreOpen = false;
    public GameObject cam;


    // Use this for initialization
    void Start () {                        
	}
	
	// Update is called once per frame
	void Update () {
        Physics.Raycast(cam.transform.position,cam.transform.forward, out hit, 2.5f);

        if (Input.GetKeyDown(KeyCode.E) && hit.transform.gameObject.tag == "Store")
        {
            StoreOpen = true;
        }
    }


    public void Damage(float damage)
    {
        Debug.Log("damaged");
        Debug.Log(health);
        health -= damage;
    }
}
