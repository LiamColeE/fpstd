﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AmmoCounter : MonoBehaviour
{
    M4 gun;
    TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        gun = this.transform.parent.parent.gameObject.GetComponent<M4>();
        text = this.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        text.SetText("Ammo:" + gun.AmmoCount());
    }
}
