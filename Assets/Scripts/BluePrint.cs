﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePrint : MonoBehaviour {

    private bool canBuild = true;
    public List<GameObject> buildingBlocks = new List<GameObject>();
    public int selectedBuildBlock = 0;
    public GameObject camera;


    // Use this for initialization
    void Start () {
        camera = this.transform.parent.parent.gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            selectedBuildBlock = 0;
        }
        else if (Input.GetKey(KeyCode.Alpha2))
        {
            selectedBuildBlock = 1;
        }
        if (Input.GetKey(KeyCode.Mouse0) && canBuild)
        {
            canBuild = false;
            Build();
        }
        else if (!Input.GetKey(KeyCode.Mouse0))
        {
            canBuild = true;
        }
    }

    private void Build()
    {
        RaycastHit col;
        LayerMask layer = 1<<selectedBuildBlock + 9;
        if (Physics.Raycast(new Vector3(camera.transform.GetChild(0).position.x, camera.transform.GetChild(0).position.y, camera.transform.GetChild(0).position.z), camera.transform.forward, out col, 4,layer))
        {
            GameObject go = GameObject.Instantiate(buildingBlocks[selectedBuildBlock]);
            go.transform.position = col.transform.position;
            go.transform.rotation = col.transform.rotation;
            Destroy(col.transform.gameObject);
        }
    }
}
