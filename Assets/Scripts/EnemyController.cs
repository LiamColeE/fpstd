﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {
    
    //Enemy stats variables
    private float health = 300;
    private float damageMod = 1;

    private NavMeshAgent enemyNav;
    private GameObject player;

    private void Start()
    {
        enemyNav = this.gameObject.GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player");
    }
    private void Update()
    {
        enemyNav.SetDestination(player.transform.position);   
    }

    public void Damage(float damage)
    {
        health -= damage * damageMod;

        if(health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("collided");
        if (other.gameObject.tag == "Player")
        {
            Attack();
        }
    }

    private void Attack()
    {
        player.GetComponent<PlayerController>().Damage(5);
    }
}
