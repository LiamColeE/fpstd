﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretPlacementController : MonoBehaviour {

    public GameObject turretPlacementPrefab;
    public GameObject turretPrefab;

    private GameObject turretPlacement;
    private GameObject turret;
    private GameObject camera;
    private bool canBuild;
    private bool placementGround;

    // Use this for initialization
    void Start () {
        camera = this.transform.parent.parent.gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        if(gameObject != null)
        {
            RaycastHit col;
            Physics.Raycast(new Vector3(camera.transform.GetChild(0).position.x, camera.transform.GetChild(0).position.y, camera.transform.GetChild(0).position.z), camera.transform.forward, out col, 8);
            if (col.transform != null && col.transform.gameObject.layer == 12)
            {
                Debug.Log("move block");
                turretPlacement.transform.position = col.point;
                placementGround = true;
            }
            else if (col.transform.tag == "Placement Snap")
            {
                turretPlacement.transform.position = col.transform.position;
                placementGround = true;
            }
            else
            {
                placementGround = false;
            }
        }

        if (Input.GetKey(KeyCode.Mouse0) && canBuild && placementGround)
        {
            canBuild = false;
            PlaceTurret();
        }
        else if (!Input.GetKey(KeyCode.Mouse0))
        {
            canBuild = true;
        }

    }

    private void OnEnable()
    {
        turretPlacement = GameObject.Instantiate(turretPlacementPrefab);
    }

    private void OnDisable()
    {
        GameObject.Destroy(turretPlacement);    
    }

    private void PlaceTurret()
    {
        turret = GameObject.Instantiate(turretPrefab);
        turret.transform.position = turretPlacement.transform.position;
        turret = null;
    }
}
