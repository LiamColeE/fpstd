﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject bullet;
    private int baseDamage = 50;
    private float shotDelay = .5f;
    private float lastShot;
    private bool lastShotSet = false;
    private GameObject enemy;
    private bool canShoot = true;

    private void Update()
    {
        if(enemy != null)
        {
            this.transform.LookAt(enemy.transform.position);
            if (canShoot)
            {
                Shoot();
                canShoot = false;
                lastShotSet = false;
            }
            else
            {
                shotTimer();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (enemy == null && other.gameObject.tag == "Enemy")
        {
            enemy = other.gameObject;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (enemy == null && other.gameObject.tag == "Enemy")
        {
            enemy = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject == enemy)
        {
            enemy = null;
        }
    }

    private void Shoot()
    {
        GameObject.Instantiate(bullet, this.transform.GetChild(0).position, this.transform.rotation);
        RaycastHit hit;
        Physics.Raycast(this.transform.position, this.transform.forward, out hit, 100f);
        if (hit.distance != 0)
        {
            if (hit.collider.gameObject.tag == "Enemy")
            {
                float damage = baseDamage / (hit.distance / 25);
                if (damage > baseDamage)
                {
                    damage = baseDamage;
                }
                hit.collider.gameObject.GetComponent<EnemyController>().Damage(damage);
            }
            else
            {
                //spawn decal at hit location
                Debug.Log("hit, not enemy");
            }
        }
    }

    private void shotTimer()
    {
        if (!lastShotSet)
        {
            lastShot = Time.time;
            lastShotSet = true;
        }

        if(Time.time - shotDelay > lastShot)
        {
            canShoot = true;
        }
    }
}
