﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject baseEnemy;
    private float lastSpawn;
    private float spawnTime = 3f;
    private bool canSpawn = true;

    private void Start()
    {
        lastSpawn = Time.time;
    }

    private void Update()
    {
        SetCanSpawn();
    }

    public void Spawn(int amount)
    {
        if (canSpawn)
        {
            for(int i = 0; i < amount; i++)
            {
                GameObject go = GameObject.Instantiate(baseEnemy);
                go.transform.position = this.gameObject.transform.position;
            }
            canSpawn = false;
            lastSpawn = Time.time;
        }
    }

    private void SetCanSpawn()
    {
        
        if ((lastSpawn + spawnTime) < Time.time)
        {
            canSpawn = true;
        }
    }
}
